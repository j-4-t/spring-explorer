package com.jdriven.springexplorer;

import com.jdriven.springexplorer.controller.GreetingController;
import com.jdriven.springexplorer.service.Greeting;
import com.jdriven.springexplorer.service.GreetingService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * [Assignment 2]
 * Test class for automating the testing of the GreetingController
 */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class GreetingControllerTests {

    /**
     * [Assignment 2]
     * Spring can provide a TestRestTemplate to simulate RESTful HTTP interaction with a servlet container.
     */
    @Autowired
    private TestRestTemplate restTemplate;

    /**
     * [Assignment 3]
     * When testing the controller in isolation, you provide a service that behaves in a predictable way so it doesn't
     * influence your test.
     *
     * Note: There are frameworks like Mockito that will mock such a dependency entirely, and even make it behave
     * exactly the way you tell it to, so a unit test isn't influenced by a change in implementation of a dependency.
     */
    private GreetingService greetingService;

    @BeforeEach
    void initMockService() {
        List<Greeting> mockGreetingList = new ArrayList<>();
        mockGreetingList.add(new Greeting(1, "one"));
        mockGreetingList.add(new Greeting(2, "two"));
        greetingService = new GreetingService(mockGreetingList);
    }

    @Test
    void greetingControllerReturnsGreetingById() {
        GreetingController controllerUnderTest = new GreetingController(greetingService);
        assertEquals(1, controllerUnderTest.getGreeting(1).getId());
        assertEquals(2, controllerUnderTest.getGreeting(2).getId());
    }

    @Test
    void greetingControllerReturnsAllGreetings() {
        GreetingController controllerUnderTest = new GreetingController(greetingService);
        assertTrue(controllerUnderTest.getGreetings().containsAll(List.of(new Greeting(1, "one"), new Greeting(2, "two"))));
    }

    @Test
    void greetingControllerRespondsToGreetingsRestCall() {
        ResponseEntity<String> response = restTemplate.getForEntity("/greetings", String.class);
        assertEquals(MediaType.APPLICATION_JSON, response.getHeaders().getContentType());
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }
}

package com.jdriven.springexplorer;

import com.jdriven.springexplorer.service.Greeting;
import com.jdriven.springexplorer.service.GreetingService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * [Assignment 3]
 * Test class for automating the testing of the GreetingService
 */
public class GreetingServiceTests {

    private List<Greeting> greetingList;

    @BeforeEach
    void initMockService() {
        greetingList = new ArrayList<>();
        greetingList.add(new Greeting(1, "three"));
        greetingList.add(new Greeting(1, "one"));
        greetingList.add(new Greeting(2, "two"));
    }
    @Test
    void greetingServiceReturnsListThatCannotBeModified() {
        GreetingService serviceUnderTest = new GreetingService(greetingList);
        assertThrows(UnsupportedOperationException.class, () -> serviceUnderTest.getAllGreetings().remove(1));
    }

    @Test
    void greetingServiceReturnsFirstGreetingById() {
        GreetingService serviceUnderTest = new GreetingService(List.of(new Greeting(1, "three"), new Greeting(1, "one"), new Greeting(1, "two")));
        assertEquals("three", serviceUnderTest.getGreetingById(1).getName());
    }
}

package com.jdriven.springexplorer.configuration;

import com.jdriven.springexplorer.service.Greeting;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;

/**
 * [Assignment 3]
 * When Spring starts, it sees classes annotated with @Configuration and looks for @Bean methods that it can instantiate
 * and inject in classes that depend on such beans. Matching beans and dependencies is done by convention on return type
 * or, if multiple candidates exist, on name.
 */
@Configuration
public class GreetingRepositoryConfiguration {

    @Bean
    public List<Greeting> greetingList() {
        Greeting g1 = new Greeting(1, "First greeting");
        Greeting g2 = new Greeting(2, "Second greeting");
        Greeting g3 = new Greeting(3, "Third greeting");
        Greeting g4 = new Greeting(3, "A second third greeting");
        return List.of(g1, g2, g3, g4);
    }

}

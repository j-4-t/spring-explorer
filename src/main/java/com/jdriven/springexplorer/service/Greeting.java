package com.jdriven.springexplorer.service;

import java.util.Objects;

/**
 * [Assignment 1]
 * Greeting class
 */
public class Greeting {

    private long id;
    private String name;

    public Greeting(long id, String name) {
        this.id = id;
        this.name = name;
    }

    public long getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    /**
     * [Assignment 2]
     * When testing whether or not a service returns the greeting you're expecting, you need to be able to tell
     * whether or not they're equal.
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Greeting greeting = (Greeting) o;
        return id == greeting.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}

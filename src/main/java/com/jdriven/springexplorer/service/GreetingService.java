package com.jdriven.springexplorer.service;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

/**
 * [Assignment 3]
 * Service for obtaining greetings
 */
@Service
public class GreetingService {

    /**
     * Use a list of greetings as the greeting repository.
     *
     * In a real world scenario, this list would most likely instead come from a database repository, which would be
     * another layer in the architecture. Spring offers various convenience adapters to communicate with well-known
     * database types.
     */
    private final List<Greeting> greetingList;

    /**
     * The only way to fill the greeting list is by providing a list of greetings upon instantiation.
     *
     * @param greetingList The list of greetings this service can provide access to
     */
    public GreetingService(List<Greeting> greetingList) {
        this.greetingList = greetingList;
    }

    /**
     * Return an unmodifiable list of the greetings this service has access to.
     *
     * @return An unmodifiable list of greetings
     */
    public List<Greeting> getAllGreetings() {
        return Collections.unmodifiableList(greetingList);
    }

    /**
     * Return the first greeting with a specific id
     *
     * @param id The greeting id
     * @return The first greeting found with the given id.
     */
    public Greeting getGreetingById(int id) {
        for (Greeting greeting : greetingList) {
            if (greeting.getId() == id) {
                return greeting;
            }
        }
        return null;
        // Using Java streams, lambdas and optionals, this could be written as:
        // return greetingRepository.stream().filter(greeting -> greeting.getId() == id).findFirst().get();
    }
}

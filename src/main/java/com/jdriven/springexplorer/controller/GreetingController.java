package com.jdriven.springexplorer.controller;

import com.jdriven.springexplorer.service.Greeting;
import com.jdriven.springexplorer.service.GreetingService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

/**
 * In the MVC architecture, a controller is what receives user interaction and presents data in models via views.
 *
 * In Web MVC, user interaction takes place via HTTP.
 * In a REST controller, the view is usually a simple JSON representation of the data in the model.
 */
@RestController
public class GreetingController {

    /**
     * [Assignment 1]
     * Variables the controller uses for generating a greeting
     */
    private static final String template = "Hello, %s!";
    private final AtomicLong counter = new AtomicLong();

    /**
     * [Assignment 1]
     * Spring Starter endpoint for returning a greeting message straight from the controller logic.
     * @param name A querystring parameter containing the name to use.
     * @return The generated greeting message.
     */
    @GetMapping("/greeting")
    public Greeting getGreeting(@RequestParam(value = "name", defaultValue = "World") String name) {
        return new Greeting(counter.incrementAndGet(), String.format(template, name));
    }

    /**
     * [Assignment 3]
     * The 'backend service' that the controller can use for obtaining information regarding greetings.
     * Typically, the controller does not know the technology used for greeting storage, nor does it know business logic
     * surrounding greetings.
     */
    private GreetingService greetingService;

    /**
     * [Assignment 3]
     * Constructor (instead of the default constructor of assignment 1) that takes a(n instance of a) greeting service
     * as an argument.
     * @param greetingService A greeting service for this controller to use.
     */
    public GreetingController(GreetingService greetingService) {
        this.greetingService = greetingService;
    }

    /**
     * [Assignment 3]
     * Obtain all greetings from the greeting service
     * @return All greetings
     */
    @GetMapping("/greetings")
    public List<Greeting> getGreetings() {
        return greetingService.getAllGreetings();
    }

    /**
     * [Assignment 3]
     * Obtain a single greeting from the greeting service
     *
     * Note: This is an overloaded method. Compare with the getGreeting(..) method for assignment 1!
     *
     * @param id The id of the greeting
     * @return The found greeting
     */
    @GetMapping("/greetings/{id}")
    public Greeting getGreeting(@PathVariable int id) {
        return greetingService.getGreetingById(id);
    }

}
